package com.link510.iniot.sdk.exception;

/**
 * 异常
 *
 * @author cqnews
 */
public class InIotException extends RuntimeException {

    private static final long serialVersionUID = 3460351796333625523L;

    private IniotStatus status;

    private String message = null;

    public InIotException(IniotStatus status) {
        this.status = status;
    }

    public InIotException(IniotStatus status, String message) {
        super(message);
        this.status = status;
        this.message = message;
    }

    public int getCode() {
        return status.getCode();
    }

    public String getError() {
        if (message != null) {
            return status.getError() + ": " + message;
        } else {
            return status.getError();
        }
    }

}
