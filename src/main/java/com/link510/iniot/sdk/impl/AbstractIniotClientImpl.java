package com.link510.iniot.sdk.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.link510.iniot.sdk.InIoTClient;
import com.link510.iniot.sdk.domain.InIotMessageInfo;
import com.link510.iniot.sdk.exception.InIotException;
import com.link510.iniot.sdk.exception.IniotStatus;
import okhttp3.*;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author cqnews
 */
public abstract class AbstractIniotClientImpl implements InIoTClient {


    public static final String DefaultMime = "application/octet-stream";
    public static final String JsonMime = "application/json";
    public static final String FormMime = "application/x-www-form-urlencoded";
    private static final String HEADER_API_APIKEY = "X-CWMAPI-ApiKey";
    private static final String HEADER_API_APISECRET = "X-CWMAPI-ApiSecret";

    private static final long serialVersionUID = 655973904085699365L;


    private final MediaType jsonMediaType = MediaType.parse(JsonMime);
    private final MediaType formMediaType = MediaType.parse(FormMime);

    private final OkHttpClient httpClient = new OkHttpClient.Builder()
            .connectTimeout(40, TimeUnit.SECONDS)
            .readTimeout(40, TimeUnit.SECONDS)
            .build();

    /**
     * 获取Api接口地址
     */
    @Override
    public String getApiUrl() {
        return "https://iot.510link.com/api";
    }

    /**
     * GET提交数据
     *
     * @param url           url地址
     * @param maps          数据提交
     * @param typeReference typeReference
     * @return 返回
     * @throws InIotException OnenetNBException
     */
    @Override
    public <T> T restPost(String url, Map<String, Object> maps, TypeReference<T> typeReference) throws InIotException {

        try {

            FormBody.Builder builder = new FormBody.Builder();

            // Add Params to Builder
            for (Map.Entry<String, Object> entry : maps.entrySet()) {
                builder.add(entry.getKey(), entry.getValue().toString());
            }

            // Create RequestBody
            RequestBody formBody = builder.build();

            Request request = new Request.Builder()
                    .url(getApiUrl() + url)
                    .post(formBody)
                    .header(HEADER_API_APIKEY, getApiKey())
                    .header(HEADER_API_APISECRET, getApiSecret())
                    .build();

            String s = handleRequest(request);

            System.out.println(s);

            return JSON.parseObject(s, typeReference);

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new InIotException(IniotStatus.HTTP_REQUEST_ERROR, "络加载失败");
        }
    }

//    /**
//     * Post提交数据
//     *
//     * @param url  url地址
//     * @param maps 参数
//     * @return 返回
//     * @throws InIotException InIotException
//     */
//    @Override
//    public <T> T restJsonPost(String url, Map<String, Object> maps, TypeReference<T> typeReference) throws InIotException {
//
//        try {
//
//            Request request = new Request.Builder()
//                    .url(getApiUrl() + url)
//                    .post(RequestBody.create(jsonMediaType, JSON.toJSONString(maps)))
//                    .header(HEADER_API_APIKEY, getApiKey())
//                    .header(HEADER_API_APISECRET, getApiSecret())
//                    .build();
//
//            String s = handleRequest(request);
//
//            return JSON.parseObject(s, typeReference);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            throw new InIotException(IniotStatus.HTTP_REQUEST_ERROR, "络加载失败");
//        }
//
//    }


    /**
     * get请求
     *
     * @param url           url地址
     * @param typeReference typeReference
     * @return 返回
     * @throws InIotException InIotException
     */
    @Override
    public <T> T restGet(String url, TypeReference<T> typeReference) throws InIotException {
        try {

            Request request = new Request.Builder()
                    .url(getApiUrl() + url)
                    .get()
                    .header(HEADER_API_APIKEY, getApiKey())
                    .header(HEADER_API_APISECRET, getApiSecret())
                    .build();

            String s = handleRequest(request);

            System.out.println(s);

            return JSONObject.parseObject(s, typeReference);

        } catch (Exception ex) {

            throw new InIotException(IniotStatus.HTTP_REQUEST_ERROR, "络加载失败");
        }
    }


//    /**
//     * get请求
//     *
//     * @param url url地址
//     * @return 返回
//     * @throws InIotException InIotException
//     */
//    @Override
//    public <T> T restDelete(String url, TypeReference<T> typeReference) throws InIotException {
//        try {
//
//            Request request = new Request.Builder()
//                    .url(getApiUrl() + url)
//                    .delete()
//                    .header(HEADER_API_APIKEY, getApiKey())
//                    .header(HEADER_API_APISECRET, getApiSecret())
//                    .build();
//
//            String s = handleRequest(request);
//
//            return JSON.parseObject(s, typeReference);
//
//        } catch (Exception ex) {
//
//            throw new InIotException(IniotStatus.HTTP_REQUEST_ERROR, "络加载失败");
//        }
//    }

    /**
     * 发起请求
     *
     * @param request 请求
     * @return 返回请求内容
     */
    private String handleRequest(Request request) throws InIotException {
        try {
            Response response = httpClient.newCall(request).execute();

            if (response != null) {

                if (!response.isSuccessful()) {
                    throw new InIotException(IniotStatus.HTTP_REQUEST_ERROR, "网络异常");
                }

                return new String(response.body().bytes(), StandardCharsets.UTF_8);

            } else {
                throw new InIotException(IniotStatus.HTTP_REQUEST_ERROR);
            }
        } catch (Exception ex) {
            System.out.println("http request error::{}" + ex.getMessage());
            ex.printStackTrace();
            throw new InIotException(IniotStatus.HTTP_REQUEST_ERROR);
        }
    }

    private <T> InIotMessageInfo<T> parseMessageInfoV1(String json) {
        return JSONObject.parseObject(json, new TypeReference<InIotMessageInfo<T>>() {
        });
    }
}
