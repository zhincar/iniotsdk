package com.link510.iniot.sdk.domain.devices;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.link510.iniot.sdk.domain.BaseDTO;
import lombok.Builder;
import lombok.Data;

import java.util.Map;

/**
 * 设备注册
 *
 * @author cqnews
 */
@Builder
@Data
public class RegisterDeviceDTO implements BaseDTO {

    private static final long serialVersionUID = 8367706666701750437L;
    /***
     * 设备Id
     */
    @Builder.Default
    private Integer id = 0;


    /**
     * 探测器名称
     */
    @Builder.Default
    private String name = "";

    /**
     * 设备imei
     * NB设备信息
     */
    @Builder.Default
    private String imei = "";
    /**
     * 设备imsi
     * NB设备信息
     */
    @Builder.Default
    private String imsi = "";
    /**
     * 设备iccid
     * NB设备信息
     */
    @Builder.Default
    private String iccid = "";

    /**
     * 设备token
     */
    @Builder.Default
    private String token = "";

    /**
     * 设备批次代码
     */
    @Builder.Default
    private String batchSN = "";


    /**
     * 产品d
     */
    @Builder.Default
    private Integer productId = 14;

    /**
     * 产品名称
     */
    @Builder.Default
    private String productName = "";

    /**
     * 产品编号
     */
    @Builder.Default
    private String productSN = "";

    /**
     * 设备模组编号
     */
    @Builder.Default
    private String moduleSN = "";


    /**
     * 转url
     *
     * @return 返回url
     */
    @Override
    public String toApiUrl() {

        StringBuilder url = new StringBuilder();

        url.append("/device/register");

        System.out.println(url);

        return url.toString();
    }

    /**
     * 转maps
     *
     * @return 返回maps
     */
    @Override
    public Map<String, Object> toMaps() {

        Map<String, Object> maps = Maps.newHashMap();

        try {

            maps.put("id", id);
            maps.put("name", name);
            maps.put("imei", imei);
            maps.put("imsi", imsi);

            if (!Strings.isNullOrEmpty(iccid)) {
                maps.put("iccid", iccid);
            }

            if (!Strings.isNullOrEmpty(token)) {
                maps.put("token", token);
            }

            if (!Strings.isNullOrEmpty(batchSN)) {
                maps.put("batchSN", batchSN);
            }

            if (productId >= 1) {
                maps.put("productId", productId);
                maps.put("productName", productName);
                maps.put("productSN", productSN);
            } else {
                maps.put("productId", 1);
                maps.put("productName", "通用设备");
                maps.put("productSN", "");
            }

            if (!Strings.isNullOrEmpty(batchSN)) {
                maps.put("moduleSN", moduleSN);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return maps;
    }
}
