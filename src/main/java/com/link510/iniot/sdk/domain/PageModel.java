package com.link510.iniot.sdk.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 分页模型
 */
@Data
@NoArgsConstructor
public class PageModel implements Serializable {

    private static final long serialVersionUID = -4156196713143165942L;
    /**
     * 每页条数
     */
    private int pageSize = 10;


    /**
     * 当前页数
     */
    private int pageNumber = 1;


    /**
     * 总页数
     */
    private int totalPages = 0;

    public PageModel(int pageSize, int pageNumber, int totalPages) {

        if (pageSize > 0) {
            this.pageSize = pageSize;
        } else {
            this.pageSize = 10;
        }

        if (pageNumber > 0) {
            this.pageNumber = pageNumber;
        } else {
            this.pageNumber = 1;
        }

        this.totalPages = totalPages;

    }

}
