package com.link510.iniot.sdk.domain.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import java.io.Serializable;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseMessageOutDTO implements Serializable {

    private static final long serialVersionUID = 6944713051661729005L;

    /**
     * 消息Id
     */
    private Integer msgId = 0;

    /**
     * 设备编号
     */
    private String deviceSN = "";

    /**
     * 消息类型
     */
    private Integer type = 0;

    /**
     * 协议名称
     */
    private String protocol = "";

    /**
     * 图片
     */
    private String litpic = "";
    /**
     * 状态
     */
    private Integer state = 0;

    /**
     * token
     */
    private String token = "";

    /**
     * 协议描述
     */
    private String protodesc = "";


    /**
     * 产品类型
     */
    private Integer productType = 0;


    /**
     * 时间
     */
    private Integer timestamp = 0;

}
