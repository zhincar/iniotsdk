package com.link510.iniot.sdk.domain.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author cqnews
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Environment11MessageOutDTO extends BaseMessageOutDTO {

    private static final long serialVersionUID = -7016045049467557511L;


    /**
     * 温度
     */
    private String temperature = "";

    /**
     * 湿度
     */
    private String humidity = "";

    /**
     * 光照强度
     */
    private String light = "";


    /**
     * UV强度
     */
    private String uvPower = "";


    /**
     * UV系数
     */
    private String uv = "";

    /**
     * 风速
     */
    private String windSpeed = "";

    /**
     * 风向
     */
    private String windDirection = "";

    /**
     * 阵风
     */
    private String windPower = "";

    /**
     * 雨量
     */
    private String rain = "";


    /**
     * 气压
     */
    private String pressure = "";

    /**
     * 气压湿度
     */
    private String pressureTemperature = "";

    /**
     * 土壤温度
     */
    private String soilTemperature = "";

    /**
     * 土壤湿度
     */
    private String soilHumidity = "";

    /**
     * 电池电压
     */
    private String batteryVoltage = "";

    /**
     * 露点
     */
    private String dewPoint = "";

    /**
     * 电容电压
     */
    private String capacitanceVoltage = "";


    /**
     * 下次时间
     */
    private String nextTime = "";

    private String activeFlag = "";
}
