package com.link510.iniot.sdk.domain.devices;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 产品信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductInfo implements Serializable {

    private static final long serialVersionUID = 1064430174390171659L;


    /**
     * 产品Id
     */
    private Integer productId = -1;


    /**
     * 产品编号
     */
    private String productSN = "";


    /**
     * 产品名称
     */
    private String name = "";

    /**
     * 分组描述
     */
    private String description = "";

    /**
     * 设备图片
     */
    private String litpic = "";

    /**
     * 生产公司
     */
    private String company = "";

    /**
     * 设备详情描述
     */
    private String body = "";

}
