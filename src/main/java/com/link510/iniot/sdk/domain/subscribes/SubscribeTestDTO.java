package com.link510.iniot.sdk.domain.subscribes;

import com.google.common.collect.Maps;
import com.link510.iniot.sdk.domain.BaseDTO;
import lombok.Builder;
import lombok.Data;

import java.util.Map;

/**
 * 设备订阅
 */
@Data
@Builder
public class SubscribeTestDTO implements BaseDTO {

    private static final long serialVersionUID = -4613644438187896518L;


    /**
     * 产品Id
     */
    private Integer productId = 0;

    /**
     * 协议
     */
    private String protocol = "";


    @Override
    public String toApiUrl() {
        return "/subscribe/test";
    }


    /**
     * 条件组装
     *
     * @return Map
     */
    @Override
    public Map<String, Object> toMaps() {

        Map<String, Object> maps = Maps.newHashMap();

        try {

            maps.put("productId", productId);
            maps.put("protocol", protocol);

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return maps;
    }
}
