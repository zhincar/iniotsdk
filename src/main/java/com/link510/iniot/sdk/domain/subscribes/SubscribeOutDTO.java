package com.link510.iniot.sdk.domain.subscribes;

import com.link510.iniot.sdk.domain.BaseDTO;
import com.link510.iniot.sdk.domain.messages.BaseMessageOutDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 设备订阅
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubscribeOutDTO implements Serializable {

    private static final long serialVersionUID = 6131423776508915098L;

    private Integer subId = 0;

    /**
     * 单位编号
     */
    private String unitCode = "";

    /**
     * 订阅名称
     */
    private String title = "";

    /**
     * 产品Id
     */
    private Integer productId = 0;

    /**
     * 协议
     */
    private String protocol = "";

    /**
     * 协议
     */
    private String httpUrl = "";

    /**
     * 订阅格式
     */
    private Integer format = 0;

    /**
     * 订阅状态
     */
    private Integer subState = 0;


    /**
     * 设备id
     */
    private String deviceId = "";


    public String apiKey = "";

    /**
     * 添加时间
     */
    private Integer addTime = 0;

    /**
     * 更新时间
     */
    private Integer updateTime = 0;
}
