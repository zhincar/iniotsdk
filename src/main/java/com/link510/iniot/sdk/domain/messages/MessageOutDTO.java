package com.link510.iniot.sdk.domain.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageOutDTO implements Serializable {

    private static final long serialVersionUID = -7329543858446003890L;


    /**
     * 消息Id
     */
    private Integer msgId = 0;

    /**
     * 设备编号
     */
    private String deviceSN = "";

    /**
     * 协议
     */
    private String protocol = "";

    /**
     * 协议描述
     */
    private String protodesc = "";

    /**
     * 状态
     */
    private Integer state = 0;

    /**
     * 时间戳
     */
    private long timestamp = 0;

    /**
     * token
     */
    private String token = "";

    /**
     * 消息类型
     */
    private Integer type = 0;

    /**
     * 用户uid
     */
    private String uid = "";
}
