package com.link510.iniot.sdk.domain.devices;

import com.google.common.collect.Maps;
import com.link510.iniot.sdk.domain.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

/**
 * 设备列表
 */
@Data
@AllArgsConstructor
public class ListDeviceDTO implements BaseDTO {


    private static final long serialVersionUID = -5043345245042243595L;

    /**
     * 每页条数
     */
    private Integer pageSize = 20;

    /**
     * 当前页数
     */
    private Integer pageNumber = 1;

    @Override
    public String toApiUrl() {

        return "/device/list";
    }


    @Override
    public Map<String, Object> toMaps() {
        Map<String, Object> maps = Maps.newHashMap();

        try {
            maps.put("pageSize", pageSize);
            maps.put("pageNumber", pageNumber);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return maps;
    }
}
