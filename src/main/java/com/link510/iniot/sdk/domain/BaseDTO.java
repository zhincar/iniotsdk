package com.link510.iniot.sdk.domain;


import java.io.Serializable;
import java.util.Map;

/**
 * @author cqnews
 */

public interface BaseDTO extends Serializable {


    /**
     * 转url
     *
     * @return 返回url
     */
    String toApiUrl();

    /**
     * 转maps
     *
     * @return 返回maps
     */
    default Map<String, Object> toMaps() {
        return null;
    }

}
