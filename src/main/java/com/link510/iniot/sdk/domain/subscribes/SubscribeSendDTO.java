package com.link510.iniot.sdk.domain.subscribes;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.link510.iniot.sdk.domain.BaseDTO;
import lombok.Builder;
import lombok.Data;

import java.util.Map;

/**
 * 设备订阅
 */
@Data
@Builder
public class SubscribeSendDTO implements BaseDTO {

    private static final long serialVersionUID = -4613644438187896518L;

    /**
     * 订阅名称
     */
    @Builder.Default
    private String title = "";

    /**
     * 产品Id
     */
    @Builder.Default
    private Integer productId = 0;

    /**
     * 协议
     */
    @Builder.Default
    private String protocol = "";

    /**
     * 协议
     */
    @Builder.Default
    private String httpUrl = "";


    /**
     * 数据格式
     */
    @Builder.Default
    private Integer format = 0;

    @Override
    public String toApiUrl() {
        return "/subscribe/send";
    }


    /**
     * 条件组装
     *
     * @return  Map
     */
    @Override
    public Map<String, Object> toMaps() {

        Map<String, Object> maps = Maps.newHashMap();

        try {

            maps.put("title", title);
            maps.put("productId", productId);
            maps.put("protocol", protocol);
            maps.put("httpUrl", httpUrl);
            maps.put("format", format);

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return maps;
    }
}
