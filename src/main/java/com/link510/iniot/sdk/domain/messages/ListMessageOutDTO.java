package com.link510.iniot.sdk.domain.messages;

import com.link510.iniot.sdk.domain.PageModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 消息列表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListMessageOutDTO implements Serializable {

    private static final long serialVersionUID = 3702339664618597306L;

    /**
     * 设备编号
     */
    private String deviceSN = "";

    /**
     * 分页模型
     */
    private PageModel pageModel;

    /**
     * 消息列表
     */
    private List<MessageOutDTO> messageInfoList;
}
