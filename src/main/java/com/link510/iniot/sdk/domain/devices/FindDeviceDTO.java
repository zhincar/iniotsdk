package com.link510.iniot.sdk.domain.devices;

import com.link510.iniot.sdk.domain.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
public class FindDeviceDTO implements BaseDTO {

    private static final long serialVersionUID = -4274550614930972535L;

    /**
     * 设备编号
     */
    private String deviceSN = "";

    private FindDeviceDTO() {
    }


    /**
     * 转url
     *
     * @return 返回url
     */
    @Override
    public String toApiUrl() {

        StringBuilder url = new StringBuilder();

        url.append("/device/show?deviceSN=").append(deviceSN);

        System.out.println(url);

        return url.toString();
    }
}
