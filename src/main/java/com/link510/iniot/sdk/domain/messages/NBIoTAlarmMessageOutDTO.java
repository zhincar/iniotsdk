package com.link510.iniot.sdk.domain.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author cqnews
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NBIoTAlarmMessageOutDTO extends BaseMessageOutDTO {

    private static final long serialVersionUID = -7016045049467557511L;


    /**
     * 报警类型
     */
    private String warningType = "B0";

    /**
     * 报警参数数量
     */
    private Integer warningParamNum = 0;

    /**
     * 报警参数
     */
    private String warningParam = "";

    /**
     * 路由等级
     */
    private Integer warningLevel = 0;

    /**
     * 报警时间
     */
    private Integer warningTime = 0;


}
