package com.link510.iniot.sdk.domain.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author cqnews
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnvironmentPmMessageOutDTO extends BaseMessageOutDTO {

    private static final long serialVersionUID = -7016045049467557511L;


    /**
     * PM1.0标准颗粒物浓度
     */
    private String standardPm10 = "";

    /**
     * PM2.5标准颗粒物浓度
     */
    private String standardPm25 = "";

    /**
     * PM10标准颗粒物浓度
     */
    private String standardPm100 = "";


    /**
     * PM1.0大气颗粒物浓度
     */
    private String airPm10 = "";


    /**
     * PM2.5大气颗粒物浓度
     */
    private String airPm25 = "";

    /**
     * PM10大气颗粒物浓度
     */
    private String airPm100 = "";

    /**
     * 直径大于0.3颗粒物个数
     */
    private String particleNum3 = "";

    /**
     * 直径大于0.5颗粒物个数
     */
    private String particleNum5 = "";

    /**
     * 直径大于1.0颗粒物个数
     */
    private String particleNum10 = "";


    /**
     * 直径大于2.5颗粒物个数
     */
    private String particleNum25 = "";

    /**
     * 直径大于5.0颗粒物个数
     */
    private String particleNum50 = "";

    /**
     * 直径大于10颗粒物个数
     */
    private String particleNum100 = "";


}
