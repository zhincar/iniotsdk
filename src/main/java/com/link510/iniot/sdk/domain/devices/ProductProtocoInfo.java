package com.link510.iniot.sdk.domain.devices;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 产品协议信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductProtocoInfo implements Serializable {


    private static final long serialVersionUID = -8021022252341815578L;

    /**
     * 协议
     */
    private String protocol = "";

    /**
     * 协议名称
     */
    private String name = "";

    /**
     * 描述
     */
    private String description = "";

    /**
     * 消息等级
     */
    private Integer msgLevel = 0;

}
