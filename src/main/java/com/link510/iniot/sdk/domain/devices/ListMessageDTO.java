package com.link510.iniot.sdk.domain.devices;

import com.google.common.collect.Maps;
import com.link510.iniot.sdk.domain.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

/**
 * 消息列表
 */
@Data
@AllArgsConstructor
public class ListMessageDTO implements BaseDTO {


    private static final long serialVersionUID = -7262608392424188470L;


    /**
     * 设备编号
     */
    private String deviceSN = "";


    /**
     * 协议
     */
    private Integer pageSize = 20;


    /**
     * 协议
     */
    private Integer pageNumber = 1;


    /**
     * 转url
     *
     * @return 返回url
     */
    @Override
    public String toApiUrl() {
        return "/message/list";
    }


    @Override
    public Map<String, Object> toMaps() {
        Map<String, Object> maps = Maps.newHashMap();

        try {
            maps.put("deviceSN", deviceSN);
            maps.put("pageSize", pageSize);
            maps.put("pageNumber", pageNumber);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        return maps;
    }
}
