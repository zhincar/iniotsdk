package com.link510.iniot.sdk.domain.devices;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 产品输出
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductOutDTO implements Serializable {

    private static final long serialVersionUID = 1064430174390171659L;

    /**
     * 产品信息
     */
    private ProductInfo productInfo;

    /**
     * 产品协议信息
     */
    private List<ProductProtocoInfo> productProtocoInfolList;

}
