package com.link510.iniot.sdk.domain.messages;

import com.link510.iniot.sdk.domain.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LastMessageDTO implements BaseDTO {

    private static final long serialVersionUID = -4274550614930972535L;

    /**
     * 设备编号
     */
    private String deviceSN = "";


    /**
     * 协议
     */
    private String protocol = "";

    private LastMessageDTO() {
    }


    /**
     * 转url
     *
     * @return 返回url
     */
    @Override
    public String toApiUrl() {

        StringBuilder url = new StringBuilder();

        url.append("/message/last?deviceSN=").append(deviceSN);
        url.append("&protocol=").append(protocol);

        System.out.println(url);

        return url.toString();
    }
}
