package com.link510.iniot.sdk.domain.subscribes;

import com.link510.iniot.sdk.domain.BaseDTO;

/**
 * 设备订阅列表
 */
public class ListSubscribeDTO implements BaseDTO {

    private static final long serialVersionUID = 6131423776508915098L;

    /**
     * 转url
     *
     * @return 返回url
     */
    @Override
    public String toApiUrl() {

        StringBuilder url = new StringBuilder();

        url.append("/subscribe/list");

        System.out.println(url);

        return url.toString();
    }
}
