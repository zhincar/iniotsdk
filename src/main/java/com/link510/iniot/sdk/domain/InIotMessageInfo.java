package com.link510.iniot.sdk.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class InIotMessageInfo<T> implements Serializable {

    private static final long serialVersionUID = -6753019300331803524L;
    /**
     * 消息分类
     */
    private Integer type = 0;


    /**
     * 消息状态
     */
    private Integer state = -1;

    /**
     * 消息说明
     */
    private String message = "英卡欢迎您";


    /**
     * 消息正文
     */
    private T content;
}
