package com.link510.iniot.sdk.domain.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author cqnews
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RFAlarmMessageOutDTO extends BaseMessageOutDTO {

    private static final long serialVersionUID = -7016045049467557511L;


    /**
     * 报警数据
     */
    private String d = "";

    /**
     * 路由等级
     */
    private String clazz = "";

    /**
     * 校验值
     */
    private String cr = "";


}
