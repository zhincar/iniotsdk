package com.link510.iniot.sdk.domain.devices;


import com.link510.iniot.sdk.domain.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;


/**
 * 查找产品
 */
@Data
@AllArgsConstructor
public class FindProductDTO implements BaseDTO {

    private static final long serialVersionUID = -6616577295038398740L;
    /**
     * 设备编号
     */
    private String deviceSN = "";

    private FindProductDTO() {
    }


    /**
     * 转url
     *
     * @return 返回url
     */
    @Override
    public String toApiUrl() {

        StringBuilder url = new StringBuilder();

        url.append("/device/product?deviceSN=").append(deviceSN);

        System.out.println(url);

        return url.toString();
    }
}
