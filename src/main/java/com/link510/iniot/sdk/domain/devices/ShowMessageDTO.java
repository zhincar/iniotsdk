package com.link510.iniot.sdk.domain.devices;

import com.link510.iniot.sdk.domain.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 设备消息
 */
@AllArgsConstructor
@Data
public class ShowMessageDTO implements BaseDTO {

    private static final long serialVersionUID = -8635674922107230429L;

    /**
     * 消息Id
     */
    private Integer msgId = 0;

    @Override
    public String toApiUrl() {

        StringBuilder url = new StringBuilder();

        url.append("/message/show?msgId=").append(msgId);

        System.out.println(url);

        return url.toString();
    }
}
