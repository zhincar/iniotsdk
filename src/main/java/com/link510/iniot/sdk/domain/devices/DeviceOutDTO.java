package com.link510.iniot.sdk.domain.devices;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 设备实体
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceOutDTO implements Serializable {


    private static final long serialVersionUID = 5591349058623231869L;


    /**
     * 设备编号
     */
    private String deviceSN = "";

    /**
     * 设备名称
     */
    private String name = "";

    /**
     * 产品Id
     */
    private Integer productId = 0;

    /**
     * 产品类型
     */
    private Integer productType = 0;

    /**
     * 纬度
     */
    private Double latitude = 0.0;

    /**
     * 经度
     */
    private Double longitude = 0.0;

    /**
     * 单位编号
     */
    private String unitCode = "";

    /**
     * 设备地址
     */
    private String address = "";

    /**
     * 设备描述信息
     */
    private String description = "";

}
