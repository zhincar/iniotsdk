package com.link510.iniot.sdk;

import com.alibaba.fastjson.TypeReference;
import com.link510.iniot.sdk.domain.InIotMessageInfo;
import com.link510.iniot.sdk.exception.InIotException;

import java.io.Serializable;
import java.util.Map;

/**
 * @author cqnews
 */
public interface InIoTClient extends Serializable {

    /**
     * GET提交数据
     *
     * @param url           url地址
     * @param typeReference
     * @return 返回
     * @throws InIotException OnenetNBException
     */
    <T> T  restPost(String url, Map<String, Object> maps, TypeReference<T> typeReference) throws InIotException;



    /**
     * GET提交数据
     *
     * @param url           url地址
     * @param typeReference
     * @return 返回
     * @throws InIotException OnenetNBException
     */
    <T> T restGet(String url, TypeReference<T> typeReference) throws InIotException;


    /**
     * 获取Api接口地址
     */
    String getApiUrl();

    /**
     * 获取ApiKey
     */
    String getApiKey();


    /**
     * 获取ApiSecret
     */
    String getApiSecret();

}
