package com.link510.iniot.sdk.management;

import com.link510.iniot.sdk.domain.InIotMessageInfo;
import com.link510.iniot.sdk.exception.InIotException;
import com.link510.iniot.sdk.exception.IniotStatus;

/**
 * 管理器
 *
 * @author cqnews
 */
public abstract class BaseManagement {


    /***
     * 消息校验
     * @param messageInfo 消息
     * @throws InIotException 解析异常
     */
    protected static void validateMessageInfo(InIotMessageInfo messageInfo) throws InIotException {

        if (messageInfo == null) {
            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }

        if (!messageInfo.getState().equals(0)) {
            throw new InIotException(IniotStatus.INIOT_CALL_ERROR, messageInfo.getMessage());
        }
    }

}
