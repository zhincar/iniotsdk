package com.link510.iniot.sdk.management;

import com.alibaba.fastjson.TypeReference;
import com.link510.iniot.sdk.InIoTClient;
import com.link510.iniot.sdk.domain.InIotMessageInfo;
import com.link510.iniot.sdk.domain.devices.*;
import com.link510.iniot.sdk.exception.InIotException;
import com.link510.iniot.sdk.exception.IniotStatus;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * 设备管理器
 *
 * @author cqnews
 */
public class DeviceManagement extends BaseManagement {

    private static final long serialVersionUID = 5470854096815664783L;


    private Lock lock = new ReentrantLock();

    /**
     * iniot策略
     */
    private InIoTClient inIoTClient = null;

    private DeviceManagement() {

    }

    /**
     * 构造方法
     *
     * @param inIoTClient inIoTClient
     */
    public DeviceManagement(InIoTClient inIoTClient) {
        this.inIoTClient = inIoTClient;
    }

    /**
     * 查找设备
     *
     * @return 返回消息
     * @throws InIotException 异常
     */
    public List<DeviceOutDTO> list() throws InIotException {
        return list(20, 1);
    }

    /**
     * 查找设备
     *
     * @return 返回消息
     * @throws InIotException 异常
     */
    public List<DeviceOutDTO> list(Integer pageSize, Integer pageNumber) throws InIotException {

        try {

            ListDeviceDTO listDeviceDTO = new ListDeviceDTO(pageSize, pageNumber);

            InIotMessageInfo<List<DeviceOutDTO>> messageInfo = this.inIoTClient.restPost(listDeviceDTO.toApiUrl(),
                    listDeviceDTO.toMaps(), new TypeReference<InIotMessageInfo<List<DeviceOutDTO>>>() {
                    });

            validateMessageInfo(messageInfo);

            List<DeviceOutDTO> list = messageInfo.getContent();

            if (list == null) {
                throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常2");
            }

            return list;

        } catch (InIotException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }

    }

    /**
     * 查找设备
     *
     * @param deviceSN 设备编号
     * @return 返回消息
     * @throws InIotException 异常
     */
    public DeviceOutDTO find(String deviceSN) throws InIotException {

        try {

            InIotMessageInfo<DeviceOutDTO> messageInfo = this.inIoTClient.restGet(new FindDeviceDTO(deviceSN).toApiUrl(), new TypeReference<InIotMessageInfo<DeviceOutDTO>>() {
            });


            validateMessageInfo(messageInfo);

            DeviceOutDTO deviceOutDTO = messageInfo.getContent();

            if (deviceOutDTO == null) {
                throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常2");
            }
            return deviceOutDTO;

        } catch (InIotException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }

    }

    /**
     * 设备注册
     *
     * @return 返回设备注册
     * @throws InIotException 自定义异常
     */
    public DeviceOutDTO register(Integer id, String name, String imei, String imsi, String token) throws InIotException {

        lock.lock();

        try {

            RegisterDeviceDTO registerDeviceDTO = RegisterDeviceDTO
                    .builder()
                    .id(id)
                    .name(name)
                    .imei(imei)
                    .imsi(imsi)
                    .token(token)
                    .build();

            return register(registerDeviceDTO);

        } catch (Exception ex) {
            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        } finally {
            lock.unlock();
        }


    }

    /**
     * 设备注册
     *
     * @return 返回设备注册
     * @throws InIotException 自定义异常
     */
    public DeviceOutDTO register(RegisterDeviceDTO registerDeviceDTO) throws InIotException {

        lock.lock();

        try {


            InIotMessageInfo<DeviceOutDTO> messageInfo = this.inIoTClient.restPost(registerDeviceDTO.toApiUrl(), registerDeviceDTO.toMaps(), new TypeReference<InIotMessageInfo<DeviceOutDTO>>() {
            });
            validateMessageInfo(messageInfo);

            DeviceOutDTO deviceOutDTO = messageInfo.getContent();

            if (deviceOutDTO == null) {
                throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常2");
            }

            return deviceOutDTO;

        } catch (InIotException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }
    }


    /**
     * 查找设备
     *
     * @param deviceSN 设备编号
     * @return 返回消息
     * @throws InIotException 异常
     */
    public ProductOutDTO product(String deviceSN) throws InIotException {

        try {

            InIotMessageInfo<ProductOutDTO> messageInfo = this.inIoTClient.restGet(new FindProductDTO(deviceSN).toApiUrl(), new TypeReference<InIotMessageInfo<ProductOutDTO>>() {
            });

            validateMessageInfo(messageInfo);

            ProductOutDTO deviceOutDTO = messageInfo.getContent();

            if (deviceOutDTO == null) {
                throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常2");
            }
            return deviceOutDTO;

        } catch (InIotException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }

    }
}
