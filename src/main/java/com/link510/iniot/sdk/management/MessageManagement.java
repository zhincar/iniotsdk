package com.link510.iniot.sdk.management;

import com.alibaba.fastjson.TypeReference;
import com.link510.iniot.sdk.InIoTClient;
import com.link510.iniot.sdk.domain.InIotMessageInfo;
import com.link510.iniot.sdk.domain.devices.ListMessageDTO;
import com.link510.iniot.sdk.domain.devices.ShowMessageDTO;
import com.link510.iniot.sdk.domain.messages.LastMessageDTO;
import com.link510.iniot.sdk.domain.messages.ListMessageOutDTO;
import com.link510.iniot.sdk.exception.InIotException;
import com.link510.iniot.sdk.exception.IniotStatus;

/**
 * 消息管理器
 *
 * @author cqnews
 */
public class MessageManagement extends BaseManagement {


    /**
     * iniot策略
     */
    private InIoTClient inIoTClient = null;

    private MessageManagement() {
    }

    public MessageManagement(InIoTClient inIoTClient) {
        this.inIoTClient = inIoTClient;
    }


    /**
     * 查找设备
     *
     * @param deviceSN 设备编号
     * @param protocol 读取协议
     * @return 返回消息
     * @throws InIotException 异常
     */
    public <T> T last(String deviceSN, String protocol, Class<T> clazz) throws InIotException {

        try {

            LastMessageDTO lastMessageDTO = new LastMessageDTO(deviceSN, protocol);

            InIotMessageInfo<T> messageInfo = this.inIoTClient.restGet(lastMessageDTO.toApiUrl(), new TypeReference<InIotMessageInfo<T>>(clazz) {
            });

            validateMessageInfo(messageInfo);

            System.out.println("messageInfo.getContent():" + messageInfo.getContent());

            T t = messageInfo.getContent();

            if (t == null) {
                throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常2");
            }

            return t;

        } catch (InIotException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }

    }

    /**
     * 获取 设备沙沙消息列表
     *
     * @param deviceSN 设备编号
     * @return T
     * @throws InIotException InIotException
     */
    public ListMessageOutDTO list(String deviceSN) throws InIotException {
        return list(deviceSN, 20, 1);

    }

    /**
     * 获取 设备沙沙消息列表
     *
     * @param deviceSN   设备编号
     * @param pageSize   每页条数
     * @param pageNumber 当前页数
     * @return T
     * @throws InIotException InIotException
     */
    public ListMessageOutDTO list(String deviceSN, Integer pageSize, Integer pageNumber) throws InIotException {
        try {

            ListMessageDTO listMessageDTO = new ListMessageDTO(deviceSN, pageSize, pageNumber);

            InIotMessageInfo<ListMessageOutDTO> messageInfo = this.inIoTClient.restPost(listMessageDTO.toApiUrl(), listMessageDTO.toMaps(), new TypeReference<InIotMessageInfo<ListMessageOutDTO>>() {
            });

            validateMessageInfo(messageInfo);

            System.out.println("messageInfo.getContent():" + messageInfo.getContent());

            ListMessageOutDTO tList = messageInfo.getContent();

            if (tList == null) {
                throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常2");
            }

            return tList;

        } catch (InIotException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }
    }

    /**
     * 获取 设备沙沙消息列表
     *
     * @param <T> 解析类
     * @return T
     * @throws InIotException InIotException
     */
    public <T> T show(Integer msgId, Class<T> clazz) throws InIotException {

        try {

            InIotMessageInfo<T> messageInfo = this.inIoTClient.restGet(new ShowMessageDTO(msgId).toApiUrl(), new TypeReference<InIotMessageInfo<T>>() {
            });

            validateMessageInfo(messageInfo);


            System.out.println("messageInfo.getContent():" + messageInfo.getContent());

            T t = messageInfo.getContent();

            if (t == null) {
                throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常2");
            }

            return t;

        } catch (InIotException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }
    }
}
