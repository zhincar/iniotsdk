package com.link510.iniot.sdk.management;


import com.alibaba.fastjson.TypeReference;
import com.link510.iniot.sdk.InIoTClient;
import com.link510.iniot.sdk.domain.InIotMessageInfo;
import com.link510.iniot.sdk.domain.messages.MessageOutDTO;
import com.link510.iniot.sdk.domain.subscribes.*;
import com.link510.iniot.sdk.exception.InIotException;
import com.link510.iniot.sdk.exception.IniotStatus;

import java.util.List;

/**
 * 消息订阅管理
 */
public class SubscribeManagement extends BaseManagement {


    /**
     * iniot策略
     */
    private InIoTClient inIoTClient = null;

    private SubscribeManagement() {
    }

    public SubscribeManagement(InIoTClient inIoTClient) {
        this.inIoTClient = inIoTClient;
    }


    /**
     * 查找设备
     *
     * @return 返回消息
     * @throws InIotException 异常
     */
    public List<SubscribeOutDTO> list() throws InIotException {

        try {

            InIotMessageInfo<List<SubscribeOutDTO>> messageInfo =
                    this.inIoTClient.restGet(new ListSubscribeDTO().toApiUrl(), new TypeReference<InIotMessageInfo<List<SubscribeOutDTO>>>() {
                    });

            validateMessageInfo(messageInfo);

            if (messageInfo.getContent() == null) {
                throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常2");
            }

            return messageInfo.getContent();

        } catch (InIotException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }

    }

    /**
     * 查找设备
     *
     * @return 返回消息
     * @throws InIotException 异常
     */
    public boolean send(String title, Integer productId, String protocol, String httpUrl, Integer format) throws InIotException {


        try {

            SubscribeSendDTO subscribeSendDTO = SubscribeSendDTO
                    .builder()
                    .title(title)
                    .productId(productId)
                    .protocol(protocol)
                    .httpUrl(httpUrl)
                    .format(format)
                    .build();

            return send(subscribeSendDTO);

        } catch (Exception ex) {
            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }
    }

    /**
     * 查找设备
     *
     * @return 返回消息
     * @throws InIotException 异常
     */
    public boolean send(SubscribeSendDTO subscribeSendDTO) throws InIotException {

        try {

            InIotMessageInfo<List<MessageOutDTO>> messageInfo =
                    this.inIoTClient.restPost(subscribeSendDTO.toApiUrl(), subscribeSendDTO.toMaps(),
                            new TypeReference<InIotMessageInfo<List<MessageOutDTO>>>() {
                            });

            validateMessageInfo(messageInfo);

            return messageInfo.getState().equals(0);

        } catch (InIotException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }

    }

    /**
     * 取消订阅
     *
     * @return 返回消息
     * @throws InIotException 异常
     */
    public boolean cancel(Integer productId, String protocol) throws InIotException {
        return cancel(SubscribeCannelDTO.builder().productId(productId).protocol(protocol).build());
    }

    /**
     * 取消订阅
     *
     * @return 返回消息
     * @throws InIotException 异常
     */
    public boolean cancel(SubscribeCannelDTO subscribeCannelDTO) throws InIotException {

        try {

            InIotMessageInfo<String> messageInfo =
                    this.inIoTClient.restPost(subscribeCannelDTO.toApiUrl(), subscribeCannelDTO.toMaps(), new TypeReference<InIotMessageInfo<String>>() {
                    });

            validateMessageInfo(messageInfo);

            return messageInfo.getState().equals(0);

        } catch (InIotException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }

    }
    /**
     * 测试消息推送
     *
     * @return 返回消息
     * @throws InIotException 异常
     */
    public boolean test(Integer productId, String protocol) throws InIotException{
        return test(SubscribeTestDTO.builder().productId(productId).protocol(protocol).build());
    }

    /**
     * 测试消息推送
     *
     * @return 返回消息
     * @throws InIotException 异常
     */
    public boolean test(SubscribeTestDTO subscribeTestDTO) throws InIotException {

        try {

            InIotMessageInfo<String> messageInfo =
                    this.inIoTClient.restPost(subscribeTestDTO.toApiUrl(), subscribeTestDTO.toMaps(), new TypeReference<InIotMessageInfo<String>>() {
                    });

            validateMessageInfo(messageInfo);

            return messageInfo.getState().equals(0);

        } catch (InIotException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new InIotException(IniotStatus.PARSER_DATA_ERROR, "数据解析异常");
        }

    }
}
