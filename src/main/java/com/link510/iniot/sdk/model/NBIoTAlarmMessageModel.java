package com.link510.iniot.sdk.model;

import com.google.common.collect.Lists;
import com.link510.iniot.sdk.domain.messages.BaseMessageOutDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author cqnews
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NBIoTAlarmMessageModel extends BaseReceiveModel {

    private static final long serialVersionUID = -7016045049467557511L;


    /**
     * 报警类型
     */
    private String warningType = "B0";

    /**
     * 报警参数数量
     */
    private Integer warningParamNum = 0;

    /**
     * 报警参数
     */
    private String warningParam = "";

    /**
     * 报警等级
     */
    private Integer warningLevel = 0;

    /**
     * 报警时间
     */
    private Integer warningTime = 0;


    /**
     * 转模型
     *
     * @return List<ProtocolRecordModel>
     */
    @Override
    public List<ProtocolRecordModel> toProtocolRecord() {

        List<ProtocolRecordModel> list = Lists.newArrayList();

        list.add(new ProtocolRecordModel("warningType", this.warningType, "报警参数数量"));
        list.add(new ProtocolRecordModel("warningParamNum", this.warningParamNum.toString(), "报警参数数量"));
        list.add(new ProtocolRecordModel("warningParam", this.warningParam, "报警参数"));
        list.add(new ProtocolRecordModel("warningLevel", this.warningLevel.toString(), "报警等级"));
        list.add(new ProtocolRecordModel("warningTime", this.warningTime.toString(), "报警时间"));

        return list;
    }
}
