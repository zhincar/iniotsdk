package com.link510.iniot.sdk.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 设备消息回调主类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class BaseReceiveModel implements Serializable {

    private static final long serialVersionUID = -4801253620453926279L;

    /**
     * 消息Id
     */
    protected Integer msgid = 0;

    /**
     * token
     */
    protected String token = "";

    /**
     * 设备编号
     */
    protected String devicesn = "";

    /**
     * 时间戳
     */
    protected Integer timestamp = 0;

    /**
     * 协议
     */
    protected String protocol = "";


    /**
     * 转模型
     *
     * @return List<ProtocolRecordModel>
     */
    public abstract List<ProtocolRecordModel> toProtocolRecord();

    @Data
    @AllArgsConstructor
    public static class ProtocolRecordModel {

        /**
         * 解析的名称
         */
        private String key = "";

        /**
         * 解析的值
         */
        private String value = "";

        /**
         * 单位
         */
        private String unit = "";

        /**
         * 协议的名称描述
         */
        private String desc = "";

        public ProtocolRecordModel(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public ProtocolRecordModel(String key, String value, String desc) {
            this.key = key;
            this.value = value;
            this.desc = desc;
        }
    }
}
