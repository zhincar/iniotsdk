package com.link510.iniot.sdk.model;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnvironmentPmReceiveModel extends BaseReceiveModel {

    private static final long serialVersionUID = 2691155760216145266L;

    /**
     * PM1.0标准颗粒物浓度
     */
    private String standardPm10 = "";

    /**
     * PM2.5标准颗粒物浓度
     */
    private String standardPm25 = "";

    /**
     * PM10标准颗粒物浓度
     */
    private String standardPm100 = "";


    /**
     * PM1.0大气颗粒物浓度
     */
    private String airPm10 = "";


    /**
     * PM2.5大气颗粒物浓度
     */
    private String airPm25 = "";

    /**
     * PM10大气颗粒物浓度
     */
    private String airPm100 = "";

    /**
     * 直径大于0.3颗粒物个数
     */
    private String particleNum3 = "";

    /**
     * 直径大于0.5颗粒物个数
     */
    private String particleNum5 = "";

    /**
     * 直径大于1.0颗粒物个数
     */
    private String particleNum10 = "";


    /**
     * 直径大于2.5颗粒物个数
     */
    private String particleNum25 = "";

    /**
     * 直径大于5.0颗粒物个数
     */
    private String particleNum50 = "";

    /**
     * 直径大于10颗粒物个数
     */
    private String particleNum100 = "";


    /**
     * 转模型
     *
     * @return List<ProtocolRecordModel>
     */
    @Override
    public List<ProtocolRecordModel> toProtocolRecord() {

        List<ProtocolRecordModel> list = Lists.newArrayList();

        list.add(new ProtocolRecordModel("standardPm10", this.standardPm10, "颗", "PM1.0标准颗粒物浓度"));
        list.add(new ProtocolRecordModel("standardPm25", this.standardPm25, "颗", "PM2.5标准颗粒物浓度"));
        list.add(new ProtocolRecordModel("standardPm100", this.standardPm100, "颗", "PM10标准颗粒物浓度"));
        list.add(new ProtocolRecordModel("airPm10", this.airPm10, "颗", "PM1.0大气颗粒物浓度"));
        list.add(new ProtocolRecordModel("airPm25", this.airPm25, "颗", "PM2.5大气颗粒物浓度"));
        list.add(new ProtocolRecordModel("airPm100", this.airPm100, "颗", "PM10大气颗粒物浓度"));
        list.add(new ProtocolRecordModel("particleNum3", this.particleNum3, "个", "直径大于0.3颗粒物个数"));
        list.add(new ProtocolRecordModel("particleNum5", this.particleNum5, "个", "直径大于0.5颗粒物个数"));
        list.add(new ProtocolRecordModel("particleNum10", this.particleNum10, "个", "直径大于1.0颗粒物个数"));
        list.add(new ProtocolRecordModel("particleNum25", this.particleNum25, "个", "直径大于2.5颗粒物个数"));
        list.add(new ProtocolRecordModel("particleNum50", this.particleNum50, "个", "直径大于5.0颗粒物个数"));
        list.add(new ProtocolRecordModel("particleNum100", this.particleNum100, "个", "直径大于10颗粒物个数"));

        return list;
    }


}
