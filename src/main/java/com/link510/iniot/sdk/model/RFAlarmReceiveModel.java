package com.link510.iniot.sdk.model;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RFAlarmReceiveModel extends BaseReceiveModel {

    private static final long serialVersionUID = 2691155760216145266L;


    /**
     * 报警数据
     */
    private String d = "";

    /**
     * 路由等级
     */
    private String clazz = "";

    /**
     * 校验值
     */
    private String cr = "";


    /**
     * 转模型
     *
     * @return List<ProtocolRecordModel>
     */
    @Override
    public List<ProtocolRecordModel> toProtocolRecord() {

        List<ProtocolRecordModel> list = Lists.newArrayList();

        list.add(new ProtocolRecordModel("d", this.d, "", "报警数据"));
        list.add(new ProtocolRecordModel("clazz", this.d, "", "路由等级"));
        list.add(new ProtocolRecordModel("cr", this.cr, "", "校验值"));

        return list;
    }


}
