package com.link510.iniot.sdk.model;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Environment11ReceiveModel extends BaseReceiveModel {

    private static final long serialVersionUID = 2691155760216145266L;

    /**
     * 温度
     */
    private String temperature = "";

    /**
     * 湿度
     */
    private String humidity = "";

    /**
     * 光照强度
     */
    private String light = "";


    /**
     * UV强度
     */
    private String uvPower = "";


    /**
     * UV系数
     */
    private String uv = "";

    /**
     * 风速
     */
    private String windSpeed = "";

    /**
     * 风向
     */
    private String windDirection = "";

    /**
     * 阵风
     */
    private String windPower = "";

    /**
     * 雨量
     */
    private String rain = "";

    /**
     * 气压
     */
    private String pressure = "";

    /**
     * 气压湿度
     */
    private String pressureTemperature = "";

    /**
     * 土壤温度
     */

    private String soilTemperature = "";

    /**
     * 土壤湿度
     */
    private String soilHumidity = "";

    /**
     * 电池电压
     */
    private String batteryVoltage = "";

    /**
     * 露点
     */
    private String dewPoint = "";

    /**
     * 电容电压
     */
    private String capacitanceVoltage = "";


    /**
     * 转模型
     *
     * @return List<ProtocolRecordModel>
     */
    @Override
    public List<ProtocolRecordModel> toProtocolRecord() {

        List<ProtocolRecordModel> list = Lists.newArrayList();

        list.add(new ProtocolRecordModel("temperature", this.temperature, "℃", "温度"));
        list.add(new ProtocolRecordModel("humidity", this.humidity, "%", "湿度"));
        list.add(new ProtocolRecordModel("light", this.light, "lux", "湿度"));
        list.add(new ProtocolRecordModel("uvPower", this.uvPower, "uw/cm2", "UV强度"));
        list.add(new ProtocolRecordModel("uv", this.uv, "", "UV系数"));
        list.add(new ProtocolRecordModel("windSpeed", this.windSpeed, "m/s", "风速"));
        list.add(new ProtocolRecordModel("windDirection", this.windDirection, "°", "风向"));
        list.add(new ProtocolRecordModel("windPower", this.windPower, "", "风力"));
        list.add(new ProtocolRecordModel("rain", this.rain, "mm/H", "雨量"));
        list.add(new ProtocolRecordModel("pressure", this.pressure, "KPa", "大气压力"));
        list.add(new ProtocolRecordModel("pressureTemperature", this.pressureTemperature, "℃", "气压湿度"));
        list.add(new ProtocolRecordModel("soilHumidity", this.soilHumidity, "%", "土壤湿度"));
        list.add(new ProtocolRecordModel("soilTemperature", this.soilTemperature, "℃", "土壤温度"));
        list.add(new ProtocolRecordModel("dewPoint", this.dewPoint, "Td", "露点"));

        return list;
    }


}
