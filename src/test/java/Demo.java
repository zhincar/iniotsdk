import com.google.common.collect.Maps;
import com.link510.iniot.sdk.InIoTClient;
import com.link510.iniot.sdk.impl.AbstractIniotClientImpl;
import com.link510.iniot.sdk.management.DeviceManagement;
import com.link510.iniot.sdk.management.MessageManagement;
import com.link510.iniot.sdk.management.SubscribeManagement;

import java.util.Map;

public class Demo {

    public static void main(String[] args) {

        InIoTClient ioTClient = new AbstractIniotClientImpl() {

            private static final long serialVersionUID = -6075989377084827143L;

            /**
             * 获取ApiKey
             */
            @Override
            public String getApiKey() {
                return "58c7f2afa3584889b198139e76856530";
            }

            /**
             * 获取ApiSecret
             */
            @Override
            public String getApiSecret() {
                return "5bb7ca57b1b80030abe4df094bfeef70";
            }
        };

        DeviceManagement deviceManagement = new DeviceManagement(ioTClient);

        SubscribeManagement subscribeManagement = new SubscribeManagement(ioTClient);

        MessageManagement messageManagement = new MessageManagement(ioTClient);

        try {

            //DeviceOutDTO data = deviceManagement.register(11, "11", "12345678", "12345678", "12345");

            //List<SubscribeOutDTO> list = subscribeManagement.list();


            //System.out.println(list);

            //boolean subscribeOutDTO = subscribeManagement.send("测试订阅", 1, "0XB0", "http://ctwing.510link.com/api/iniot/receive", 1);


            //System.out.println(subscribeOutDTO);

            //ProductOutDTO productOutDTO = deviceManagement.product("75001");

            //System.out.println(productOutDTO);

//            ListMessageOutDTO outDTO = messageManagement.list("75001", 20, 1);
//
//            System.out.println(outDTO);4590375

//
//            Environment11MessageOutDTO messageOutDTO = messageManagement.last("75001", "0000", Environment11MessageOutDTO.class);

            Map<String, String> map = messageManagement.show(4590375, Maps.newHashMap().getClass());

            System.out.println(map);

        } catch (Exception ex) {

            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }

    }
}
